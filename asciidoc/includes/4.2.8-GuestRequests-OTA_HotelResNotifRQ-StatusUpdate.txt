<HotelReservations>

    <HotelReservation ResStatus="Reserved" CreateDateTime="2023-03-21T11:00:00+01:00">
        <UniqueID Type="14" ID="6b34fe24ac999999"/>
    </HotelReservation>

    <HotelReservation ResStatus="Cancelled" CreateDateTime="2023-05-12T19:00:00+01:00">
        <UniqueID Type="15" ID="6b34fe24ac244444"/>
    </HotelReservation>

</HotelReservations>
