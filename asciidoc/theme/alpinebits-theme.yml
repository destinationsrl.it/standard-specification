font:
  catalog:
    Liberation Sans:
      normal: LiberationSans-Regular.ttf
      bold: LiberationSans-Bold.ttf
      italic: LiberationSans-Italic.ttf
      bold_italic: LiberationSans-BoldItalic.ttf
    Bitstream Vera Sans Mono:
      normal: VeraMono.ttf
      bold: VeraMoBd.ttf
      italic: VeraMoIt.ttf
      bold_italic: VeraMoBI.ttf
page:
  background_color: ffffff
  layout: portrait
  margin: [15mm, 15mm, 12.7mm, 15mm]
  # margin_inner and margin_outer keys are used for recto/verso print margins when media=prepress
  margin_inner: 0.75in
  margin_outer: 0.59in
  size: A4
base:
  align: left
  # color as hex string (leading # is optional)
  font_color: 000
  font_family: Liberation Sans
  font_size: 11
  line_height_length: 12
  line_height: $base_line_height_length / $base_font_size
  font_size_large: round($base_font_size * 1.25)
  font_size_small: round($base_font_size * 0.85)
  font_size_min: $base_font_size * 0.75
  font_style: normal
  border_color: eeeeee
  border_radius: 0
vertical_rhythm: $base_line_height_length * 3 / 4
horizontal_rhythm: $base_line_height_length
# QUESTION should vertical_spacing be block_spacing instead?
vertical_spacing: $vertical_rhythm
link:
  font_color: 428bca
# literal is currently used for inline monospaced in prose and table cells
literal:
  font_color: $base_font_color
  font_family: Bitstream Vera Sans Mono
  font_size: 11
heading:
  align: left
  font_color: $base_font_color
  font_family: $base_font_family
  font_style: bold
  # h1 is used for part titles (book doctype only)
  h1_font_size: floor($base_font_size * 2.6)
  # h2 is used for chapter titles (book doctype only)
  h2_font_size: 18
  h3_font_size: 14
  h4_font_size: $base_font_size_large
  h5_font_size: $base_font_size
  h6_font_size: $base_font_size * 1.1
  line_height: 1
  margin_top: $vertical_rhythm * 0.4
  margin_bottom: $vertical_rhythm * 0.9
title_page:
  align: right
  logo:
    top: 10%
  title:
    top: 55%
    font_size: $heading_h1_font_size
    font_color: 999999
    line_height: 0.9
  subtitle:
    font_size: $heading_h3_font_size
    font_style: bold_italic
    line_height: 1
  authors:
    margin_top: $base_font_size * 1.25
    font_size: $base_font_size_large
    font_color: 181818
  revision:
    margin_top: $base_font_size * 1.25
block:
  margin_top: 0
  margin_bottom: $vertical_spacing
caption:
  align: left
  font_size: $base_font_size * 0.95
  font_style: italic
  # FIXME perhaps set line_height instead of / in addition to margins?
  margin_inside: $vertical_rhythm / 3
  margin_outside: 0
lead:
  font_size: $base_font_size_large
  line_height: 1.4
abstract:
  font_color: 5c6266
  font_size: $lead_font_size
  line_height: $lead_line_height
  font_style: italic
  first_line_font_style: bold
  title:
    align: center
    font_color: $heading_font_color
    font_family: $heading_font_family
    font_size: $heading_h4_font_size
    font_style: $heading_font_style
blockquote:
  font_color: $base_font_color
  font_size: $base_font_size_large
  border_color: $base_border_color
  border_width: 5
  cite_font_size: $base_font_size_small
  cite_font_color: 999999
# code is used for source blocks (perhaps change to source or listing?)
code:
  font_color: $base_font_color
  font_family: $literal_font_family
  line_height: 1
  font_size: 9
  # line_gap is an experimental property to control how a background color is applied to an inline block element
  line_gap: 3.8
  border_radius: $base_border_radius
  border_width: 0
conum:
  font_family: M+ 1mn
  font_color: $literal_font_color
  font_size: $base_font_size
  line_height: 4 / 3
example:
  border_color: $base_border_color
  border_radius: $base_border_radius
  border_width: 0.75
  background_color: ffffff
image:
  align: left
prose:
  margin_top: $block_margin_top
  margin_bottom: $vertical_spacing
sidebar:
  background_color: eeeeee
  border_color: e1e1e1
  border_radius: $base_border_radius
  border_width: $base_border_width
  # FIXME reenable padding bottom once margin collapsing is implemented
  padding: [$vertical_rhythm, $vertical_rhythm * 1.25, 0, $vertical_rhythm * 1.25]
  title:
    align: center
    font_color: $heading_font_color
    font_family: $heading_font_family
    font_size: $heading_h4_font_size
    font_style: $heading_font_style
thematic_break:
  border_color: $base_border_color
  border_style: solid
  border_width: $base_border_width
  margin_top: $vertical_rhythm * 0.5
  margin_bottom: $vertical_rhythm * 1.5
description_list:
  term_font_style: bold
  term_spacing: $vertical_rhythm / 4
  description_indent: $horizontal_rhythm * 1.25
outline_list:
  indent: $horizontal_rhythm * 1.5
  # NOTE outline_list_item_spacing applies to list items that do not have complex content
  item_spacing: $vertical_rhythm / 2
table:
  background_color: $page_background_color
  head_background_color: #f3f3f3
  head_font_style: bold
  body_stripe_background_color: ffffff
  foot_background_color: f0f0f0
  border_color: 333333
  border_width: $base_border_width
  cell_padding: 3
toc:
  indent: $horizontal_rhythm
  line_height: 1.4
  h2_font_style: bold
  dot_leader:
    content: " "
    font_color: a9a9a9
    #levels: 2 3
# NOTE in addition to footer, header is also supported
footer:
  font_size: 10
  # NOTE if background_color is set, background and border will span width of page
  height: $base_line_height_length * 2.5
  line_height: 1
  padding: [$base_line_height_length / 2, 1, 0, 1]
  vertical_align: top
  recto:
    left:
      content: '{AlpineBitsR} {AlpineBitsVersion}'
    right:
      content: 'page {page-number} of {page-count}'
  verso:
    left:
      content: $footer_recto_left_content
    right:
      content: $footer_recto_right_content
small:
  font_size: 9


## Following properties require the AlpineBits branch of asciidoctor and asciidoctor-pdf
price_algo_calc_variable:
  font_color: 74a2ed
  font_style: bold
request_param:
  font_color: 6aa84f
  font_family: $literal_font_family
generic_bkg:
  background_color: #f3f3f3
freerooms_bkg:
  font_family: $literal_font_family
  text_background_color: #cfe2f3
  background_color: $freerooms_bkg_text_background_color
guestrequests_bkg:
  font_family: $literal_font_family
  text_background_color: #d9ead3
  background_color: $guestrequests_bkg_text_background_color
simplepackages_bkg:
  font_family: $literal_font_family
  text_background_color: #fff2cc
  background_color: $simplepackages_bkg_text_background_color
inventory_bkg:
  font_family: $literal_font_family
  text_background_color: #d0e0e3
  background_color: $inventory_bkg_text_background_color
rateplans_bkg:
  font_family: $literal_font_family
  text_background_color: #fce5cd
  background_color: $rateplans_bkg_text_background_color
baserates_bkg:
  font_family: $literal_font_family
  text_background_color: #ead1dc
  background_color: $baserates_bkg_text_background_color
activity_bkg:
  font_family: $literal_font_family
  text_background_color: #d5d3ea
  background_color: $activity_bkg_text_background_color
xml_element_name:
  font_color: #ff0000
  font_family: $literal_font_family
  font_style: bold
xml_attribute_name:
  font_color: #600000
  font_family: $literal_font_family
  font_style: bold
